#!/usr/bin/python3
from kubernetes import client,config 
from flask import Flask

#get the token from the secret mount
aToken = open('/run/secrets/kubernetes.io/serviceaccount/token').readline().split('\n')[0]

#Configs can be set in Configuration class directly or using helper utility 
configuration = client.Configuration() 
configuration.host="https://kubernetes.default.svc.cluster.local"
configuration.verify_ssl= False
configuration.api_key={"authorization":"Bearer "+ aToken}
client.Configuration.set_default(configuration)
#configuration.debug = True


app = Flask(__name__)

@app.route("/", methods=["GET"])
def home():
#get the deployments in the default namespace  
   apis_api = client.AppsV1Api()
   resp = apis_api.list_namespaced_deployment(namespace="default") 

#prepare the output HTML web page
   output = """<!DOCTYPE html>
<html>
   <head>
      <title>Kubernetes Deployments</title>
   </head>
   <body style="background-color:blue;">
"""


   if len(resp.items) > 0:
     output += "<h1> Here are the deployments in the default namespace: </h1>"
     for i in resp.items:
        output += "<h2> - " + i.metadata.name + "</h2>"
   else: 
     output += "<h1> No deployments in the default namespace ! </h1>"
   output += "</body></html>"
   return(output)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
